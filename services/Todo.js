/**
  * to render todos first time from database
  */
const get = () => fetch("http://localhost:3000/");

const add = (currentTodo) => fetch("http://localhost:3000/add", {
  method: "POST",
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({
    "todo" : currentTodo
  })
});

const update = (item) => fetch("http://localhost:3000/update", {
  method: "POST",
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({
    "_id" : item._id,
    "completed" : !item.completed
  })
});

const del = (item) => fetch("http://localhost:3000/delete", {
  method: "DELETE",
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({
    "_id" : item._id
  })
});

export default {
  get,
  add,
  del,
  update
}