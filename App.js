import React , { useState, useEffect } from 'react';
import { StyleSheet, Text, View , SafeAreaView , TextInput , Button , ScrollView, ActivityIndicator } from 'react-native';
import TodoService from './services/Todo';
import Todo from './components/todo';
import ErrorBoundary from './components/errorboundary';
import _ from 'lodash';

export default function App() {
  /**
   * states for adding a todo and to the array of all todos
   */
  const [ currentTodo , setTodo ] = useState('');
  const [ currentTodos , setTodos ] = useState([]);
  const [ fetchingTodos , setFetching ] = useState(false);
  
  // text input handler
  const textInputHandler = (todoString) => {
    setTodo(todoString);
  }
  
  /**
   * to render todos first time from database
   */
  const fetchTodos = () => {
    setFetching(true);

    TodoService.get()
      .then(response => response.json())
      .then(responseJSON => {//console.log(responseJSON);
        const todos = responseJSON;
        setTodos(todos);
        setFetching(false);
      }).catch(err => {
        alert('something went wrong',err);
        setFetching(false);
      });
  }

  /**
   *  to render the todos from database at first render
   */
  useEffect(() => {
    fetchTodos();
  }, []);

  /**
   * to add a new todo and store in database
   */
  const onClickHandler = () => {
    //let flag = 0;
    setFetching(true);
    if(currentTodo === "") {
      alert("Please enter a todo");
      setFetching(false);
      return;
    }
    // TO check duplicate todos
    for(let i=0 ; i<currentTodos.length ; i++){
      if( currentTodos[i].todo.toLowerCase() === currentTodo.toLowerCase()){
        alert('Entered todo already exist');
        setFetching(false);
        setTodo('');
        return;
      }
    }

    TodoService.add(currentTodo)
    .then((response) => response.json())
    .then(responseJSON => {
      if(responseJSON.message !== "todo is inserted") {
        alert("something went wrong");
        setFetching(false);
        return;
      } 
      else {
        setTodos([...currentTodos, { 
          _id : responseJSON.todo._id,
          todo : responseJSON.todo.todo,
          completed : responseJSON.todo.completed
        }]);
        setFetching(false);
        setTodo('');
      }
    })
    .catch(err => {
      alert(err);
      setFetching(false);
      setTodo('');
    });
  }

/**
 * to mark the todo completed or not
 */
  const checkBoxHandler = (index) => {
    const todos = _.cloneDeep(currentTodos);

    const item = todos[index];
    TodoService.update(item)
    .then( response => response.json())
    .then( responseJSON => {
      item.completed = !item.completed;
      setTodos(todos);
    } ).catch(err => alert('something went wrong',err));
  }

  /**
   * to delete the todo on click of 'X'
   */
  const onDeleteHandler = (index) => {
    const todos = _.cloneDeep(currentTodos);
    const item = todos[index];

    TodoService.del(item)
    .then( response => response.json())
    .then( responseJSON => {
      todos.splice(index,1);
      setTodos(todos);
    } ).catch(err => alert('something went wrong',err));
  }

  return (
    <SafeAreaView style={styles.SafeAreaViewStyle}>
      <ErrorBoundary>
        <View style={styles.headerContainer}>
          <Text style = {styles.header}>TODO</Text>
        </View>

        <View style = {styles.bodyContainer1}>
          <View style = {styles.formContainer}>
            <TextInput
              style={styles.TextInput} 
              value={currentTodo}
              placeholder = " Enter a todo..." onChangeText={textInputHandler}/>
            <Text>{' '}</Text>
            <Button title="Add" color="black" backgroundColor="black" onPress={onClickHandler}/>
          </View>

          <View style={{backgroundColor:"black", padding : 10}}>
            {
              fetchingTodos && <ActivityIndicator size="large" color="white" animating={fetchingTodos} /> 
            }
          </View>

          <ScrollView style={styles.Scroll}>
          {
              currentTodos.map( (item, i) => {
                  return (
                    <Todo key={i} item={item} index={i} onComplete={checkBoxHandler} onDelete={onDeleteHandler} />
                  );
              })
            }
          </ScrollView>

        </View>
      </ErrorBoundary>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  headerContainer: {
    flex: 1,
    backgroundColor: 'black',
    flexDirection : "column",
    alignItems: 'center',
    justifyContent: 'center',
  },
  header:{ padding : 2,color : "white" , fontSize : 30 , fontWeight : "bold"},
  bodyContainer1 : {
    flex : 8,
    backgroundColor : "#fff"
  },
  bodyContainer2 : {
    flex : 8,
    backgroundColor : "#e9e9e9",
    flexDirection : "column",
    alignItems : "center",
    justifyContent : "center",
    borderStyle : "solid",
    borderColor : "black",
    borderWidth : 7,
    padding : 7
  },
  SafeAreaViewStyle:{
    flex : 1, backgroundColor : "black"
  },
  formContainer : {padding : 5, backgroundColor : "white", flexDirection : "row"},
  TextInput : { height: 35, borderColor: 'gray', borderWidth: 1 , color : "black" , flex : 3},
  Scroll : {backgroundColor:"black", padding : 2},
});