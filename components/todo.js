import React from 'react';
import {View, Text, Button} from 'react-native';
import {CheckBox} from 'react-native-elements';

export default function Todo({ item, index, onComplete, onDelete }) {
    return (
        <View key={index} style={{ margin: 2,padding : 2,backgroundColor : "#fff" , borderWidth : 0.6 , 
            borderColor : "black",flex : "auto", 
            flexDirection:"row", alignItems: 'center', justifyContent: 'space-between' }}>
  
          <View style={{flex : 2}}>
            <CheckBox
              center
              size={20}
              checkedColor="green"
              checked={item.completed}
              onPress={() => onComplete(index)}
            />
          </View>
              
          <View style={{flex:8}} >
            { item.completed && <Text  onPress={() => onComplete(index)} style={{textDecorationLine:"line-through"} }>{item.todo}</Text> }
            { !item.completed && <Text onPress={() => onComplete(index)}>{item.todo}</Text> }
          </View>
          
          <View style={{flex:2}}>
            <Button title="X" color="red" onPress ={() => onDelete(index)}/>
          </View>
        </View>
    );
  }