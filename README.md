**WHY TODO APP?**

TODO app was developed as a part of react native learning. This app uses MongoDB to store the TODOs and update their states or delete them. CRUD operations can be performed with this app, concepts of useState, useEffect, Checkbox, ScrollView, SafeAreaView,etc were implemented in this. 

**Note:** 
*  This app will work when both frontend and backend servers are up and running.
*  Please add .env file in backend with your database URL for this to work.
    `mongoURI=<your mongoDB URI and change the password>`
*  Please make necesssary changes in the fetch api's based on the port number you are running your backend.
    example : `fetch("http://localhost:3000/")` if you are runnning the backend in port 3000

**USER STORIES:**

As a user I want to add a TODO, mark it complete, change the state to completed or not if I got to recheck, or delete if not needed anymore.
I want TODO to be available even if I am closing the app and re-using in the future time.

**ACCEPTANCE CRITERIA:**

*  Test if app alerts user trying to insert an empty TODO.
*  Test if text input is getting cleared after submit.
*  Test if app displays some activity if there is delay in fetching or updating from backend.
*  Test if user can add a TODO.
*  Test if user can mark the TODO completed or not at anytime.
*  Test if user can delete a TODO when it is completed and not needed.
*  Test if TODOs added in past are rendered when user closes the app and opens in future time.
*  Test if user is alerted when network connectivity is lost.
*  Test if app alerts user if they tried to add duplicate todos.
